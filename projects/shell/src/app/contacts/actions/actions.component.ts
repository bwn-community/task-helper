import { Location } from '@angular/common';
import { Component } from '@angular/core';
import { ActivatedRoute, ActivationEnd, Router, UrlSerializer } from '@angular/router';
import { ContactService } from 'projects/shell/src/state/services/contact.service';
import { filter, map, shareReplay, switchMap, tap } from 'rxjs/operators';
import { DialogSerivce } from '../../dialogs/dialog.service';

@Component({
  selector: 'sh-actions',
  templateUrl: './actions.component.html',
  styleUrls: ['./actions.component.scss'],
})
export class ActionsComponent {
  public assigneeId$ = this.router.events
    .pipe(
      filter(e => (e instanceof ActivationEnd) && (Object.keys(e.snapshot.params).length > 0)),
      map(e => e instanceof ActivationEnd ? e.snapshot.params : {}),
      map(({ contactId }) => contactId),
      tap(console.log),
    );

  public primaryUser$ = this.contactService.primaryUser$;

  constructor(
    public router: Router,
    public dialog: DialogSerivce,
    public activatedRoute: ActivatedRoute,
    private contactService: ContactService,
  ) {}
}
