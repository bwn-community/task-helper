import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ActionsComponent } from './actions/actions.component';
import { ContactsComponent } from './contacts.component';
import { CreateComponent } from './create/create.component';
import { DetailsComponent } from './details/details.component';

const routes: Routes = [
  {
    path: "",
    children: [
      {
        path: "member",
        component: ContactsComponent,
        children: [
          {
            path: "create",
            data: { title: "Create profile", type: 'create', goBack: true },
            component: CreateComponent,
          },
          {
            path: ":contactId/edit",
            data: { title: "Edit profile", type: 'edit', goBack: true },
            component: CreateComponent,
          },
          {
            path: "",
            data: { title: "Team details", goBack: true },
            component: DetailsComponent,
          },
          {
            path: ":contactId",
            data: { title: "Team", goBack: false },
            component: DetailsComponent,
          },
        ]
      },
      {
        path: "",
        outlet: "topbarActions",
        component: ActionsComponent
      },
      {
        pathMatch: "full",
        path: "**",
        redirectTo: "member"
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContactsRoutingModule {}
