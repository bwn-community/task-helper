import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ContactService } from '../../state/services/contact.service';

@Component({
  selector: 'sh-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent {

  public readonly contacts$ = this.contactService.contacts$;
  public readonly primaryUser$ = this.contactService.primaryUser$;

  constructor(
    public route: ActivatedRoute,
    private contactService: ContactService,
  ) { }

}
