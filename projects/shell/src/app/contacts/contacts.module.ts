import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactsRoutingModule } from './contacts-routing.module';
import { ContactsComponent } from './contacts.component';
import { DetailsComponent } from './details/details.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ActionsComponent } from './actions/actions.component';
import { MatMenuModule } from '@angular/material/menu';
import { MatDividerModule } from '@angular/material/divider';
import { MatTabsModule } from '@angular/material/tabs';
import { CreateComponent } from './create/create.component';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { propertyFilter } from './details/property-filter.pipe';

import { MatSelectModule } from '@angular/material/select';
import { FilterComponent } from './details/filter/filter.component';
import { FilterService } from './details/filter/filter.service';

@NgModule({
  declarations: [ContactsComponent, DetailsComponent, ActionsComponent, CreateComponent, propertyFilter, FilterComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ContactsRoutingModule,
    MatButtonModule,
    MatIconModule,
    MatTooltipModule,
    MatMenuModule,
    MatDividerModule,
    MatTabsModule,
    MatInputModule,
    MatSelectModule,
  ],
  providers: [FilterService],
})
export class ContactsModule { }
