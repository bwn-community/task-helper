import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { contact, contacts } from 'projects/shell/src/state/selectors';
import { ContactService } from 'projects/shell/src/state/services/contact.service';
import { TaskService } from 'projects/shell/src/state/services/task.service';
import { StoreNS } from 'projects/shell/src/state/types/store.interface';
import { delay, filter, map, switchMap } from 'rxjs/operators';

@Component({
  selector: 'sh-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent {
  @ViewChild('contact')
  public contact: NgForm;

  public primaryUser$ = this.contactService.primaryUser$;

  public contact$ = this.route.params.pipe(
    map(({ contactId }) => contactId),
    filter((id) =>!!id),
    switchMap((id) => {
      return this.store.pipe(select(contact, { id }));
    }),
  );

  constructor(
    public route: ActivatedRoute,
    private router: Router,
    private store: Store<StoreNS.Object>,
    private contactService: ContactService,
  ) { }

  ngOnInit() {
    this.contact$.pipe(delay(20)).subscribe((contact) => {
      this.contact.form.patchValue(contact);
    });
  }

  public create() {
    const contact = this.contact.form.value;
    this.contactService.create(contact);

    this.router.navigateByUrl(`/team/member/${contact.id}`);
  }

  public save() {
    const contact = this.contact.form.value;
    this.contactService.update(contact);

    this.router.navigateByUrl(`/team/member/${contact.id}`);
  }

  public remove() {
    this.contactService.remove(this.contact.form.value);

    this.router.navigateByUrl(`/team/member`);
  }
}
