import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { select } from '@ngrx/store';
import { ofAssignee, ofId } from 'projects/shell/src/state/selectors';
import { ContactService } from 'projects/shell/src/state/services/contact.service';
import { ProjectService } from 'projects/shell/src/state/services/project.service';
import { TaskService } from 'projects/shell/src/state/services/task.service';
import { TaskNS } from 'projects/shell/src/state/types/task.interface';
import { filter, switchMap, withLatestFrom } from 'rxjs/operators';
import { FilterService } from './filter/filter.service';

@Component({
  selector: 'sh-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  public readonly taskTypes = TaskNS.Type;
  public readonly taskStatuses = TaskNS.Status;
  public readonly taskComplexity = TaskNS.Complexity;

  public readonly filters$ = this.filterService.filters$;

  public contact$;

  public tasks$ = this.activatedRoute.params.pipe(
    filter(({ contactId }) => !!contactId),
    switchMap(({ contactId }) => {
        return this.taskService.allTasks$.pipe(
          select(ofAssignee, { assigneeId: contactId }),
        );
    }),
  );

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private contactService: ContactService,
    private taskService: TaskService,
    private filterService: FilterService,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params
      .pipe(withLatestFrom(this.contactService.primaryUser$))
      .subscribe(([{ contactId }, primaryUser]) => {
        if (!contactId) {
          this.router.navigateByUrl(`/team/member/${primaryUser.id}`);
        } else {
          this.contact$ = this.contactService.contacts$.pipe(select(ofId, { id: contactId }));
        }
      });
  }

  complexityFrom(value: string): string {
    if (value !== undefined && value !== null) {
      return TaskNS.Complexity[value].replace(/\D/g, '');
    } else {
      return "";
    }
  }

}
