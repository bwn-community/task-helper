import { AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { ProjectService } from 'projects/shell/src/state/services/project.service';
import { TaskService } from 'projects/shell/src/state/services/task.service';
import { TaskNS } from 'projects/shell/src/state/types/task.interface';
import { filter as filterOption } from '../property-filter.pipe';
import { BehaviorSubject, of } from 'rxjs';
import { FilterService } from './filter.service';
import { delay, take } from 'rxjs/operators';

@Component({
  selector: 'sh-task-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
})
export class FilterComponent implements AfterViewInit {
  @ViewChild('form')
  public form: NgForm;

  public readonly taskStatuses = TaskNS.Status;

  public projects = new FormControl();
  public projects$ = this.projectService.projects$;

  public statuses = new FormControl();
  public statuses$ = of(this.taskService.statuses);

  public readonly filters$ = this.filterService.filters$;

  constructor(
    private taskService: TaskService,
    private projectService: ProjectService,
    private filterService: FilterService,
  ) { }

  ngAfterViewInit(): void {
    this.filterService.filters$.pipe(take(1), delay(10)).subscribe(filters => {
      const filterValues = filters.reduce((values, filter) => {
        values[filter.property] = filter.value;
        return values;
      }, {});

      this.form.form.patchValue(filterValues);
    });

    this.form.form.valueChanges.subscribe(filters => {
      Object
        .keys(filters)
        .filter(property => Array.isArray(filters[property]))
        .forEach(property => {
          this.filterService.remove(property);
          const operation = this.getFilterOperation(property);
          const value = filters[property];
          if (value.length !== 0) {
            this.filterService.add(property, operation, value);
          } else {
            this.filterService.remove(property);
          }
        });
    });
  }

  private getFilterOperation(property: string): string {
    return {
      project: 'in',
      status: 'in',
    }[property];
  }
}
