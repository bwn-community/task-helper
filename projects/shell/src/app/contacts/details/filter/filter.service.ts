import { Injectable } from "@angular/core";
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class FilterService {
    private _filters$ = new BehaviorSubject([]);

    public filters$ = this._filters$.asObservable();

    public add(property, operation, value) {
        const filters = this._filters$.getValue();
        const index = filters.findIndex(filter => filter.property === property);

        if (index === -1) {
            this._filters$.next(filters.concat([{ property, operation, value }]));
        } else {
            filters[index] = { property, operation, value };
            this._filters$.next([...filters]);
        }
    }

    public remove(property) {
        const filters = this._filters$.getValue();
        const index = filters.findIndex(filter => filter.property === property);

        this._filters$.next(filters.filter(filter => filter.property !== property));
    }

    public reset() {
        this._filters$.next([]);
    }
}