import { Pipe, PipeTransform } from "@angular/core";
import { TaskNS } from 'projects/shell/src/state/types/task.interface';

export interface filter {
    property: 'status' | 'project';
    value: any;
    operation?: '==' | '!=' | 'in' | '!in';
}

export const FILTER_OPERATORS = {
    '==': (a, b) => a === b,
    '!=': (a, b) => a !== b,
    'in': (a, b) => b.includes(a),
    '!in': (a, b) => !b.includes(a),
};

@Pipe({ name: 'propertyFilter' })
export class propertyFilter implements PipeTransform {
    public transform(tasks: TaskNS.Object[], filters: filter[]) {
        if (filters === void 0 || Object.keys(filters).length === 0) return tasks;

        return filters.reduce((tasks, filter) => {
            const operation = FILTER_OPERATORS[filter.operation || '=='];

            return tasks.filter(task => operation(task[filter.property], filter.value));
        }, tasks);
    }
}