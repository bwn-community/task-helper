import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KanbanActionsComponent } from './actions.component';

describe('KanbanActionsComponent', () => {
  let component: KanbanActionsComponent;
  let fixture: ComponentFixture<KanbanActionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KanbanActionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KanbanActionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
