import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { take } from 'rxjs/operators';
import { DialogSerivce } from '../../dialogs/dialog.service';
import { HistoryService } from '../../../state/services/history.service';
import { ProjectService } from '../../../state/services/project.service';
import { ProjectNS } from '../../../state/types/project.interface';
import { StoreNS } from '../../../state/types/store.interface';
import { KanbanPortService } from '../store/port.service';

@Component({
  selector: 'kanban-actions',
  templateUrl: './actions.component.html',
  styleUrls: ['./actions.component.scss']
})
export class KanbanActionsComponent {
  public downloadStateUri: string;

  public readonly projects$ = this.projectService.projects$;
  public readonly selectedProject$ = this.projectService.selectedProject$;

  constructor(
    private projectService: ProjectService,
    public portService: KanbanPortService,
    public dialog: DialogSerivce,
    private store: Store<StoreNS.Object>,
    public history: HistoryService,
    private router: Router,
  ) {}

  public downloadState() {
    this.portService.export();
  }

  public removeSelectedProject() {
    this.dialog.open('confirmDelete', {
      title: "Delete Project",
      question: "Are you sure you want to permanently delete this project? Doing so will also delete all associated tasks."
    }).afterClosed().subscribe((action) => {
      if (action === "confirm") {
        this.projectService.selectedProject$.pipe(take(1)).subscribe((project) => {
          this.projectService.remove(project);
          this.router.navigateByUrl('/kanban');
        });
      }
    });
  }

  public renameProject() {
    this.projectService.selectedProject$
      .pipe(take(1))
      .subscribe((project) => {
        this.dialog.open('renameProject', {
          type: 'Project',
          question: `What would you like to rename ${project} to?`,
          initalValue: project
        });
      });
  }

  public selectProject(project: ProjectNS.Value) {
    this.projectService.select(project);
  }

  private navigateBack(params) {
    if (params.project) {
      this.projectService.select(params.project);
    }

    this.router.navigateByUrl(params.returnTo || '/kanban');
  }
}
