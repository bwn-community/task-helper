import { NgModule } from '@angular/core';
import { Routes, RouterModule, ɵEmptyOutletComponent } from '@angular/router';
import { KanbanActionsComponent } from './actions/actions.component';
import { BacklogComponent } from './pages/backlog/backlog.component';
import { BoardComponent } from './pages/board/board.component';
import { TaskComponent } from './pages/task/task.component';

const routes: Routes = [
  {
    path: "",
    children: [
      {
        path: "",
        outlet: "topbarActions",
        component: KanbanActionsComponent
      },
      {
        path: "",
        data: { title: "Tasks" },
        component: BoardComponent
      },
      {
        path: "backlog",
        data: { title: "Backlog", goBack: true },
        component: BacklogComponent
      },
      {
        path: "task",
        children: [
          {
            path: "",
            data: { title: "Task create", goBack: true },
            component: TaskComponent,
          },
          {
            path: ":id",
            data: { title: "Task details", goBack: true },
            component: TaskComponent,
          }
        ]
      },
      {
        pathMatch: "full",
        path: "**",
        redirectTo: ""
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class KanbanRoutingModule {}
