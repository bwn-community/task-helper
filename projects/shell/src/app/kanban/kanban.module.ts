import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KanbanRoutingModule } from './kanban-routing.module';

// Material Design
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatListModule } from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';
import { MatBadgeModule } from '@angular/material/badge';
import { MatInputModule } from '@angular/material/input';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatDialogModule } from "@angular/material/dialog";
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatTabsModule } from '@angular/material/tabs';

import { KanbanActionsComponent } from './actions/actions.component';
import { BoardComponent } from './pages/board/board.component';
import { TaskComponent } from './pages/task/task.component';
import { FormsModule } from '@angular/forms';
import { BacklogComponent } from './pages/backlog/backlog.component';
import { ComplexityPipe } from './pipes/complexity.pipe';
import { KanbanPortService } from './store/port.service';

@NgModule({
  declarations: [BoardComponent, BacklogComponent, KanbanActionsComponent, TaskComponent, ComplexityPipe],
  imports: [
    CommonModule,
    FormsModule,
    KanbanRoutingModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatGridListModule,
    MatCardModule,
    MatListModule,
    MatBadgeModule,
    MatInputModule,
    MatExpansionModule,
    MatTooltipModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatAutocompleteModule,
    MatDialogModule,
    DragDropModule,
    MatTabsModule,
  ],
  providers: [KanbanPortService],
  bootstrap: [BoardComponent],
})
export class KanbanModule { }
