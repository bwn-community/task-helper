import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProjectService } from 'projects/shell/src/state/services/project.service';
import { TaskService } from 'projects/shell/src/state/services/task.service';
import { TaskNS } from '../../types/task.interface';

@Component({
  selector: 'sh-backlog',
  templateUrl: './backlog.component.html',
  styleUrls: ['./backlog.component.scss']
})
export class BacklogComponent {
  public readonly taskTypes = TaskNS.Type;
  public readonly taskComplexity = TaskNS.Complexity;

  public backlog$ = this.taskService.backlog$;

  constructor(
    public route: ActivatedRoute,
    private taskService: TaskService,
  ) {}

  public drop(event: CdkDragDrop<string[]>) {
    const task: TaskNS.Object = event.previousContainer.data[event.previousIndex] as any;
    const bellowTask = event.container.data[event.currentIndex];
    const aboveTask = event.container.data[event.currentIndex - 1];

    this.taskService.move(
      task,
      bellowTask,
      aboveTask,
      "Backlog",
    );
  }
}
