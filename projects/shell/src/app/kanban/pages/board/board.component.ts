import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { Component, HostListener, ViewChild } from '@angular/core';
import { MatMenuTrigger } from '@angular/material/menu';
import { ActivatedRoute } from '@angular/router';
import { select } from '@ngrx/store';
import { contact } from 'projects/shell/src/state/selectors';
import { ContactService } from 'projects/shell/src/state/services/contact.service';
import { ProjectService } from 'projects/shell/src/state/services/project.service';
import { TaskService } from 'projects/shell/src/state/services/task.service';
import { tap } from 'rxjs/operators';
import { DialogSerivce } from '../../../dialogs/dialog.service';
import { TaskNS } from '../../types/task.interface';

@Component({
  selector: 'sh-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardComponent {
  public readonly taskTypes = TaskNS.Type;
  public readonly taskStatuses = TaskNS.Status;
  public readonly taskComplexity = TaskNS.Complexity;

  public readonly types = Object.keys(TaskNS.Type)
    .filter(k => typeof TaskNS.Type[k as any] === "number");

  public readonly statuses = Object.keys(TaskNS.Status)
    .filter(k => typeof TaskNS.Status[k as any] === "number");

  public readonly complexities = Object.keys(TaskNS.Complexity)
    .filter(k => typeof TaskNS.Complexity[k as any] === "number");

  @ViewChild("contextMenuTrigger", { static: true })
  public contextMenu: MatMenuTrigger;

  public contextMenuPosition = { x: "0px", y: "0px" };

  public todo$ = this.taskService.todo$;
  public inProgress$ = this.taskService.inProgress$;
  public review$ = this.taskService.review$;
  public done$ = this.taskService.done$;

  public contacts$ = this.contactService.contacts$;

  public projects$ = this.projectService.projects$;
  public selectedProject$ = this.projectService.selectedProject$;

  constructor(
    public route: ActivatedRoute,
    private contactService: ContactService,
    private taskService: TaskService,
    private projectService: ProjectService,
    private dialog: DialogSerivce,
  ) {}

  @HostListener("window:contextmenu", ["$event"])
  public preventContextMenu(event: MouseEvent) {
    event.stopPropagation();
    event.preventDefault();
  }

  public getContactById(contacts, id: string) {
    return [...(contacts || [])].find(contact => contact.id === id);
  }

  public getInitals(name: string): string {
    return name.split(' ').map(word => word.charAt(0)).join('');
  }

  public openContextMenu(event: MouseEvent, task, rowName) {
    this.preventContextMenu(event);
    this.contextMenuPosition.x = event.clientX + "px";
    this.contextMenuPosition.y = event.clientY + "px";
    this.contextMenu.menuData = { task, rowName };
    this.contextMenu.menu.focusFirstItem("mouse");
    this.contextMenu.openMenu();
  }

  public updateTask(task, property, value) {
    this.taskService.update({
      ...task,
      [property]: value
    });
  }

  public deleteTask(task: TaskNS.Object) {
      this.dialog.open('confirmDelete', {
        title: "Delete Task",
        question: "Are you sure you want permanently delete this task?"
      }).afterClosed().subscribe((action) => {
        if (action === "confirm") {
          this.taskService.remove(task);
        }
      });
  }


  public drop(event: CdkDragDrop<string[]>) {
    const task: TaskNS.Object = event.previousContainer.data[event.previousIndex] as any;
    const bellowTask = event.container.data[event.currentIndex];
    const aboveTask = event.container.data[event.currentIndex - 1];

    const toRowName = event.container.element.nativeElement.getAttribute('name');

    this.taskService.move(
      task,
      bellowTask,
      aboveTask,
      toRowName,
    );
  }
}
