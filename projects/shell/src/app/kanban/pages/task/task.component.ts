import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { v4 as uuid } from 'uuid';
import { ActivatedRoute, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { TaskNS } from '../../types/task.interface';
import { StoreNS } from '../../types/kanban.interface';
import {
  delay,
  filter,
  map,
  switchMap,
  take,
  withLatestFrom,
} from 'rxjs/operators';
import { ProjectService } from 'projects/shell/src/state/services/project.service';
import { TaskService } from 'projects/shell/src/state/services/task.service';
import { notesOfTask, tasks } from 'projects/shell/src/state/selectors';
import { NoteService } from 'projects/shell/src/state/services/note.service';
import { DialogSerivce } from '../../../dialogs/dialog.service';
import { ContactService } from 'projects/shell/src/state/services/contact.service';

@Component({
  selector: 'sh-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss'],
})
export class TaskComponent {
  public tab = 'details';

  @ViewChild('task', { static: true })
  public taskForm: NgForm;

  public projects$ = this.projectService.projects$;
  public assignees$ = this.taskService.assignees$;
  public readonly primaryUser$ = this.contactService.primaryUser$;

  public notes$ = this.noteService.notes$;

  public readonly taskTypes = Object.keys(TaskNS.Type).filter(
    (type) => typeof TaskNS.Type[type] === 'number'
  );
  public readonly taskStatuses = Object.keys(TaskNS.Status).filter(
    (status) => typeof TaskNS.Status[status] === 'number'
  );
  public readonly taskComplexities = Object.keys(TaskNS.Complexity).filter(
    (complexity) => typeof TaskNS.Complexity[complexity] === 'number'
  );

  constructor(
    public route: ActivatedRoute,
    private contactService: ContactService,
    private store: Store<StoreNS.Root>,
    private dialog: DialogSerivce,
    private projectService: ProjectService,
    private taskService: TaskService,
    private noteService: NoteService
  ) {}

  ngAfterViewInit() {
    this.projectService.selectedProject$.pipe(take(1)).subscribe((project) => {
      setTimeout(() => {
        this.taskForm.form.patchValue({ project });
      });
    });

    this.route.params
      .pipe(
        take(1),
        filter(({ id }) => !id),
        filter(({ status, assignee }) => status || assignee),
        delay(20)
      )
      .subscribe((params) => {
        this.taskForm.form.patchValue({
          assignee: params.assignee ? params.assignee : undefined,
          status: params.status ? TaskNS.Status[params.status] : undefined,
        });
      });

    this.route.params
      .pipe(
        take(1),
        filter(({ id }) => !id),
        withLatestFrom(this.contactService.primaryUser$),
        delay(10)
      )
      .subscribe(([params, primaryUser]) => {
        this.taskForm.form.patchValue({
          assignee: primaryUser.id,
        });
      });

    this.route.params
      .pipe(
        take(1),
        filter(({ id }) => !!id),
        switchMap(({ id }) =>
          this.store.pipe(
            select(tasks),
            map((tasks) => tasks.find((task) => task.id === id))
          )
        ),
        filter((task) => !!task),
        delay(10)
      )
      .subscribe((task) => {
        this.notes$ = this.store.pipe(select(notesOfTask, { taskId: task.id }));
        this.taskForm.form.patchValue({
          ...task,
          status: TaskNS.Status[task.status],
          type: TaskNS.Type[task.type],
          complexity: TaskNS.Complexity[task.complexity],
        });
      });
  }

  update(task) {
    this.taskService.update({
      ...task,
      type: task.type ? TaskNS.Type[task.type] : undefined,
      complexity: task.complexity
        ? TaskNS.Complexity[task.complexity]
        : undefined,
      status: task.status ? TaskNS.Status[task.status] : undefined,
      flagged: task.flagged === '' ? false : task.flagged,
      hidden: task.hidden === '' ? false : task.hidden,
    });

    this.navigateBack({ project: task.project });
  }

  remove(task: TaskNS.Object) {
    this.dialog
      .open('confirmDelete', {
        title: 'Delete Task',
        question: 'Are you sure you want permanently delete this task?',
      })
      .afterClosed()
      .subscribe((action) => {
        if (action === 'confirm') {
          this.taskService.remove(task);
          this.navigateBack({ project: task.project });
        }
      });
  }

  create({ createMultiple, ...task }) {
    this.taskService.create({
      ...task,
      id: uuid(),
      type: task.type ? TaskNS.Type[task.type] : undefined,
      complexity: task.complexity
        ? TaskNS.Complexity[task.complexity]
        : undefined,
      status: task.status ? TaskNS.Status[task.status] : undefined,
      flagged: task.flagged === '' ? false : task.flagged,
      hidden: task.hidden === '' ? false : task.hidden,
    } as any);

    if (createMultiple) {
      this.taskForm.resetForm({ createMultiple, project: task.project });
    } else {
      this.navigateBack({ project: task.project });
    }
  }

  public createNote(noteForm, event) {
    event.preventDefault();
    const { note } = noteForm.value;
    const task = this.taskForm.value;

    this.noteService.add({
      message: note,
      project: task.project,
      taskId: task.id
    });

    noteForm.resetForm();
  }

  private navigateBack(params) {
    if (params.project) {
      this.projectService.select(params.project);
    }

    history.back();
  }
}
