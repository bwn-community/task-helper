import { Pipe, PipeTransform } from '@angular/core';
import { TaskNS } from '../types/task.interface';

@Pipe({
  name: 'complexity'
})
export class ComplexityPipe implements PipeTransform {

  transform(value: string): string {
    if (value !== undefined && value !== null) {
      return TaskNS.Complexity[value].replace(/\D/g, '');
    } else {
      return "";
    }
  }
}