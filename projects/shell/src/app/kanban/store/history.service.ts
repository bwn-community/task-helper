import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Actions, createEffect } from "@ngrx/effects";
import { BehaviorSubject } from 'rxjs';
import { filter, take, tap } from 'rxjs/operators';
import { taskReducer } from './task.reducer';
import { undoState, redoState } from './kanban.actions';
import { noteReducer } from './note.reducer';
import { projectReducer } from './project.reducer';
import { selectedProjectReducer } from './selected-project.reducer';

const pastActions = JSON.parse(window.sessionStorage.getItem('task-helper.pastActions') || "[]");
const futureActions = JSON.parse(window.sessionStorage.getItem('task-helper.futureActions') || "[]");
const initalState = JSON.parse(window.sessionStorage.getItem('task-helper.initalState'));

@Injectable()
export class KanbanHistoryService {
    public pastActions = new BehaviorSubject(pastActions);
    public futureActions = new BehaviorSubject(futureActions);

    private initalState = initalState;
    private stateReducers = [
        { reducer: taskReducer, property: "tasks" },
        { reducer: noteReducer, property: "notes" },
        { reducer: projectReducer, property: "projects" },
        { reducer: selectedProjectReducer, property: "selectedProject" },
    ];

    public saveState = createEffect(
    () =>
        this.actions$.pipe(
            filter(({ type }) => type !== "[kanban] Undo state action"),
            filter(({ type }) => type !== "@ngrx/effects/init"),
            tap((action) => {
                if (action.type !== "[kanban] Redo state action") {
                    this.futureActions.next([]);
                    this.push(action);
                }
            })
        ),
        {
            dispatch: false,
        }
    );

    constructor(
        private actions$: Actions,
        private store: Store<any>,
    ) {
        this.futureActions.subscribe((data) => this.persist("futureActions", data));
        this.pastActions.subscribe((data) => this.persist("pastActions", data));

        if (!initalState) {
            this.store.pipe(take(1)).subscribe((state) => {
                this.initalState = state;
                this.persist("initalState", state);
            })
        }
    }

    public back() {
        this.pop();
        const history = this.pastActions.getValue();

        const fragments = this.stateReducers.map(({ reducer, property }) => {
            const state = this.initalState[property];

            return {
                [property]: history.reduce(reducer, state),
            };
        });

        this.store.dispatch(
            undoState(Object.assign({}, ...fragments)),
        );
    }

    public forward() {
        this.shift();
        const history = this.pastActions.getValue();

        const fragments = this.stateReducers.map(({ reducer, property }) => {
            const state = this.initalState[property];

            return {
                [property]: history.reduce(reducer, state),
            };
        });

        this.store.dispatch(
            redoState(Object.assign({}, ...fragments)),
        );
    }

    private push(action) {
        const array = this.pastActions.getValue().slice(0);
        array.push(action);
        this.pastActions.next(array);
    }

    private pop() {
        const array = this.pastActions.getValue().slice(0);
        const removed = array.pop();
        this.unshift(removed);
        this.pastActions.next(array);

        return removed;
    }

    private shift() {
        const array = this.futureActions.getValue().slice(0);
        const removed = array.pop();
        this.push(removed);
        this.futureActions.next(array);

        return removed;
    }

    private unshift(action) {
        const array = this.futureActions.getValue().slice(0);
        array.push(action);
        this.futureActions.next(array);
    }

    private persist(dataType: "futureActions" | "pastActions" | "initalState", data = []) {
        window.sessionStorage.setItem(`task-helper.${dataType}`, JSON.stringify(data));
    }
}