import { Injectable } from '@angular/core';
import { saveAs } from 'file-saver';

@Injectable()
export class KanbanPortService {
    public export() {
        const state = window.localStorage.getItem("task-helper.state");
        const selectedProject = window.localStorage.getItem("task-helper.selectedProject");

        const blob = new Blob([
            `{ "selectedProject": "${selectedProject}", "state": ${state}  }`
        ], {type: "application/json;charset=utf-8"});
      
        saveAs(blob, "task-helper.json");
    }

    public import() {
        return new Promise(resolve => {
            const uploader = document.createElement('input');
            uploader.type = 'file';
            uploader.accept = 'application/json';
            uploader.hidden = true;

            document.body.appendChild(uploader);

            uploader.addEventListener('change', (event: any) => {
                const { files } = event.target;
                const reader = new FileReader();
                
                let output = "";
          
                reader.onload = function(){
                  var text = reader.result;
                  output += text;
                  resolve(output);
                  document.body.removeChild(uploader);
                };
          
          
                reader.readAsText(files[0], 'utf-8');
            });

            uploader.click();
        }).then(JSON.parse).then(({ state, selectedProject }) => {
            window.localStorage.setItem("task-helper.state", JSON.stringify(state));
            window.localStorage.setItem("task-helper.selectedProject", selectedProject);
            window.location.reload();
        });
    }
}