
export namespace HistoryNS {
    export interface Service {
        back(): void;
        forward(): void;
    }

    export interface Object {}
}