import { NoteNS } from './note.interface';
import { TaskNS } from './task.interface';

export namespace StoreNS {
    export interface Root extends Kanban {
        selectedProject: string;
    }

    export interface Kanban {
        tasks: TaskNS.Object[];
        notes: NoteNS.Object[];
        projects: string[];
    };
}

