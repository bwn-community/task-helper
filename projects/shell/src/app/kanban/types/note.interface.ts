export namespace NoteNS {
    export enum Type {
        "user",
        "action",
    }

    export interface Object {
        id: string;
        taskId: string;
        message: string;
        created: Date;
        type?: Type;
    }
}