export type kanban = TaskNS.Object[];
export type markdown = string;

export namespace TaskNS {

    export enum Type {
        "Bug",
        "Story",
        "Feature",
        "Spike",
        "Refactor",
        "Task",
    }

    export enum Status {
        "Todo",
        "In progress",
        "Review",
        "Done",
        "Backlog",
    }

    export enum Complexity {
        "1 point",
        "2 points",
        "3 points",
        "5 points",
        "8 points",
        "13 points",
    }

    export enum Severity {
        "Lowest",
        "Low",
        "Medium",
        "High",
        "Critical"
    }

    export interface Object {
        id: string;
        title: string;
        description: markdown;
    
        project: string;
        projectId?: string;

        status: Status;
        type?: Type;
        complexity?: Complexity;
        severity?: Severity;

        start?: Date;
        end?: Date;

        created?: Date;
        updated?: Date;

        flagged?: boolean;
        hidden?: boolean;
    }
}