import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { articles } from 'projects/shell/src/state/selectors';
import { StoreNS } from 'projects/shell/src/state/types/store.interface';
import { map, switchMap, tap } from 'rxjs/operators';
import * as marked from 'marked';

@Component({
  selector: 'sh-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {
  private readonly _marked: marked = marked;

  public articles$ = this.store.pipe(select(articles));

  public article$ = this.route.params.pipe(
    switchMap(({ titleSlug }) => {
      return this.articles$.pipe(map((articles) => {
        return articles.find(article => article.slug === titleSlug);
      }));
    }),
  );

  constructor(
    public route: ActivatedRoute,
    private store: Store<StoreNS.Object>,
  ) { }

  ngOnInit(): void {
    // this.marked("[[Home page]] is going to be [[fun]]!")
  }

  public marked(content, artciles = []) {
    let html = this._marked(content);

    const reg = new RegExp(/\[\[(.*?)\]\]/, 'g');
    let result = reg.exec(html);
    
    while (result) {
      const article = artciles.find(article => article.title === result[1]);

      if (article) {
        html = html.replace(result[0], `<a class="link--article" href="/wiki/${article.slug}">${result[1]}</a>`);
      } else {
        html = html.replace(result[0], `<a class="link--create" href="/wiki/${result[1]}/edit">${result[1]}</a>`);
      }
      

      result = reg.exec(html);
    }

    console.log(html);

    return html;
  }
}
