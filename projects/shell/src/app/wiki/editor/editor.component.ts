import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { articles } from 'projects/shell/src/state/selectors';
import { StoreNS } from 'projects/shell/src/state/types/store.interface';
import { delay, map, switchMap, take } from 'rxjs/operators';
import * as marked from 'marked';
import { NgForm } from '@angular/forms';
import { ArticleService } from 'projects/shell/src/state/services/article.service';

@Component({
  selector: 'sh-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss']
})
export class EditorComponent implements OnInit {
  @ViewChild('articleForm')
  public articleForm: NgForm;
  
  public readonly marked: marked = marked;

  public titleSlug$ = this.route.params.pipe(map(({ titleSlug }) => titleSlug));
  
  public article$ = this.titleSlug$.pipe(
    switchMap((titleSlug) => {
      return this.store.pipe(select(articles), map((articles) => {
        return articles.find(article => article.slug === titleSlug) || {
          slug: titleSlug,
          title: "New document",
          content: "",
        };
      }));
    }),
  );

  constructor(
    public route: ActivatedRoute,
    public articleService: ArticleService,
    private store: Store<StoreNS.Object>,
  ) { }

  ngOnInit(): void {
    this.article$.pipe(take(1), delay(20)).subscribe(article => {
      this.articleForm.form.patchValue(article);
    })
  }

  public save(article: NgForm, content) {
    this.articleService.update({
      ...content,
      ...article.form.value,
      updated: new Date().toISOString(),
    });

    history.back();
  }

  public create(article: NgForm) {
    this.articleService.create({
      ...article.form.value,
      created: new Date().toISOString(),
      updated: new Date().toISOString(),
    });

    history.back();
  }

}
