import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, ɵEmptyOutletComponent } from '@angular/router';
import { ArticleComponent } from './article/article.component';
import { EditorComponent } from './editor/editor.component';

export const routes = [
  {
    path: ":titleSlug",
    data: { title: 'Documentation' },
    component: ArticleComponent
  },
  {
    path: ":titleSlug/edit",
    data: { title: 'Edit article', goBack: true },
    component: EditorComponent
  },
  {
    pathMatch: "full",
    path: "**",
    redirectTo: "wiki"
  },
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule]
})
export class WikiRoutingModule { }
