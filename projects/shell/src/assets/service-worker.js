importScripts(
    "https://storage.googleapis.com/workbox-cdn/releases/5.1.2/workbox-sw.js"
  );
  
  workbox.routing.registerRoute(
    ({ request }) =>
      request.destination === "script" ||
      request.destination === "style" ||
      request.destination === "document" ||
      request.destination === "manifest" ||
      request.destination === "image" ||
      request.destination === "font",
    new workbox.strategies.NetworkFirst({
      cacheName: "static-resources",
    })
  );
  