import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { DialogSerivce } from '../../app/dialogs/dialog.service';
import { projects } from '../../state/selectors';
import { StoreNS } from '../../state/types/store.interface';

@Component({
  selector: 'settings-actions',
  templateUrl: './actions.component.html',
  styleUrls: ['./actions.component.scss']
})
export class SettingsActionsComponent {
  public projects$ = this.store.pipe(select(projects));
  public downloadStateUri: string;

  constructor(
    private store: Store<StoreNS.Object>,
    private router: Router,
    public dialog: DialogSerivce,
  ) {}

  public removeSelectedProject() {}

  private navigateBack(params) {
    this.router.navigateByUrl(params.returnTo || '/kanban');
  }
}
