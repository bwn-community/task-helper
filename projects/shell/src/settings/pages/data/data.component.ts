import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { DialogSerivce } from 'projects/shell/src/app/dialogs/dialog.service';
import { KanbanPortService } from 'projects/shell/src/app/kanban/store/port.service';
import { removeProject } from 'projects/shell/src/state/actions/project.actions';
import { map } from 'rxjs/operators';

@Component({
  selector: 'sh-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.scss'],
  providers: [KanbanPortService]
})
export class DataComponent implements OnInit {

  public projects$ = this.store.pipe(map(({ projects }) => projects));

  constructor(
    public route: ActivatedRoute,
    public port: KanbanPortService,
    public store: Store<any>,
    public dialog: DialogSerivce,
  ) { }

  ngOnInit(): void {}

  public removeProject(project) {
    this.dialog.open('confirmDelete', {
      title: "Delete Project",
      question: "Are you sure you want to permanently delete this project? Doing so will also delete all associated tasks."
    }).afterClosed().subscribe((action) => {
      if (action === "confirm") {
        this.store.dispatch(removeProject({ project }));
      }
    });
  }

  public renameProject(project) {
    this.dialog.open('renameProject', {
      type: 'Project',
      question: `What would you like to rename ${project} to?`,
      initalValue: project
    });
  }

}
