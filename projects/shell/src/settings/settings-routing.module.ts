import { NgModule } from '@angular/core';
import { Routes, RouterModule, ɵEmptyOutletComponent } from '@angular/router';
import { SettingsActionsComponent } from './actions/actions.component';
import { DataComponent } from './pages/data/data.component';
import { SettingsComponent } from './settings.component';

const routes: Routes = [
    {
      path: "",
      outlet: "topbarActions",
      component: SettingsActionsComponent
    },
    {
        path: "",
        component: SettingsComponent,
        children: [
          {
            path: "data",
            component: DataComponent,
          },
          {
            path: "spelling",
            data: { name: "Spell check settings" },
            component: ɵEmptyOutletComponent
          },
          {
            path: "appearance",
            data: { name: "Appearance settings" },
            component: ɵEmptyOutletComponent
          },
          {
            path: "**",
            pathMatch: "full",
            redirectTo: "data"
          }
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule {}
