import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DialogSerivce } from '../app/dialogs/dialog.service';

@Component({
  selector: 'settings-root',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent {
  constructor(
    public route: ActivatedRoute,
    private router: Router,
    private dialog: DialogSerivce,
  ) {}

  public resetApp() {
    this.dialog.open('confirmDelete', {
      title: "Reset application",
      question: "Are you sure you want to reset your app? Doing so will delete all your data."
    }).afterClosed().subscribe((action) => {
      if (action === "confirm") {
        this.router.navigateByUrl("/onboarding");
        // this.store.dispatch(removeProject({ project }));
      }
    });
  }
}
