import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsComponent } from './settings.component';
import { SettingsRoutingModule } from './settings-routing.module';
import { SettingsActionsComponent } from './actions/actions.component';

import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatTabsModule } from '@angular/material/tabs';
import { MatListModule } from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';
import { DataComponent } from './pages/data/data.component';
import { StoreModule } from '@ngrx/store';

@NgModule({
  declarations: [SettingsComponent, SettingsActionsComponent, DataComponent],
  imports: [
    CommonModule,
    SettingsRoutingModule,
    MatMenuModule,
    MatButtonModule,
    MatIconModule,
    MatDividerModule,
    MatTabsModule,
    MatCardModule,
    MatListModule,
    StoreModule.forFeature("settings", {}),
  ],
  bootstrap: [SettingsComponent],
})
export class SettingsModule { }
