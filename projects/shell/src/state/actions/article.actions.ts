import { createAction, props } from '@ngrx/store';
import { ArticleNS } from '../types/article.interface';

export const createArticle = createAction("[task-helper] Create article", props<{ article: ArticleNS.Object }>());
export const updateArticle = createAction("[task-helper] Update article", props<{ article: ArticleNS.Object }>());