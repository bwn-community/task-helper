import { createAction, props } from '@ngrx/store';
import { ContactNS } from '../types/contact.interface';

export const createEvent = createAction("[task-helper] Create event", props<{}>());
export const updateEvent = createAction("[task-helper] Create event", props<{}>());
export const removeEvent = createAction("[task-helper] Remove event", props<{}>());
export const repeatEvent = createAction("[task-helper] Repeat event");