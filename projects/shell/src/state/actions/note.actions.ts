import { createAction, props } from "@ngrx/store";
import { NoteNS } from '../types/note.interface';

// Note Actions
export const addNote = createAction("[task-helper] Add note", props<NoteNS.Object>());
export const removeNote = createAction("[task-helper] Remove note", props<NoteNS.Object>());
