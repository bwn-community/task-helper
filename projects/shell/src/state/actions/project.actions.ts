import { createAction, props } from "@ngrx/store";
import { ProjectNS } from '../types/project.interface';

// Project Actions
export const selectProject = createAction("[task-helper] Select project", props<{ project: ProjectNS.Value }>());
export const removeProject = createAction("[task-helper] Remove project", props<{ project: ProjectNS.Value }>());
export const renameProject = createAction("[task-helper] Rename project", props<{ from: ProjectNS.Value, to: ProjectNS.Value }>());
export const createProject = createAction("[task-helper] Create project", props<{ project: ProjectNS.Value }>());