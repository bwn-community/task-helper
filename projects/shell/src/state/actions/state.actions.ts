import { createAction, props } from "@ngrx/store";
import { StoreNS } from '../types/store.interface';

// State Actions
export const undoAction = createAction("[task-helper] Undo action", props<StoreNS.Object>());
export const redoAction = createAction("[task-helper] Redo action", props<StoreNS.Object>());
