import { ContactNS } from './contact.interface';

export namespace ArticleNS {
    export type slug = string;
    export type markdown = string;
    export type dateString = string;

    export interface Object {
        title: string;
        slug: slug;

        readonly?: boolean;

        redirectTo?: slug;

        authors: ContactNS.ID[];
        contents: markdown;

        created: dateString;
        updated: dateString;
    }
}