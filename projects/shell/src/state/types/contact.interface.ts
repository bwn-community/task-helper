import { ProjectNS } from './project.interface';

export namespace ContactNS {
    export type ID = string;

    export type CreateObject = Omit<Object, 'id' | 'tasks'>;

    export interface Object {
        id: ID;
        name: string;
        email?: string;
        title?: string;

        directManager?: ID;

        primaryUser?: boolean;

        proejcts?: ProjectNS.Value[];

        tasks: string[];
    }
}