export namespace EventNS {
    export type ID = string;

    export interface Object {
        id: ID;
    }
}