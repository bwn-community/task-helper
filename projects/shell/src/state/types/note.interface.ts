export namespace NoteNS {
    export type ID = string;
    
    export type CreateObject = Omit<NoteNS.Object, 'id' | 'created'>;

    export interface Object {
        id: ID;
        message: string;
        project: string;
        taskId: string;
        created: string;
    }
}