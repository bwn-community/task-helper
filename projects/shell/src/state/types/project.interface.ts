export namespace ProjectNS {
    export type ID = string;

    export type Selected = string;

    export type Value = string;
}