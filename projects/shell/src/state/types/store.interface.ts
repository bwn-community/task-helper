import { ArticleNS } from './article.interface';
import { ContactNS } from './contact.interface';
import { NoteNS } from './note.interface';
import { ProjectNS } from './project.interface';
import { TaskNS } from './task.interface';

export namespace StoreNS {
    export interface Object {
        selectedProject: ProjectNS.Selected;
        projects: ProjectNS.Value[];
        tasks: TaskNS.Object[];
        notes: NoteNS.Object[];
        contacts: ContactNS.Object[];
        articles: ArticleNS.Object[];
    }
}